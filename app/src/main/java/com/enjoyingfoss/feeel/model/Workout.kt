package com.enjoyingfoss.feeel.model

import android.os.Parcel
import android.os.Parcelable

/**
@author Miroslav Mazel
 */
class Workout(val titleResource: Int,
              val customColor: Long,
              val exerciseMetas: Array<ExerciseMeta>,
              val breakLength: Int) : Parcelable {

    /**
     * customColor encoded as (A & 0xff) << 24 | (R & 0xff) << 16 | (G & 0xff) << 8 | (B & 0xff), can reference with e.g. 0xff0000ff
     */

    val size: Int
        get() = exerciseMetas.size

    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readLong(),
            parcel.createTypedArray(ExerciseMeta),
            parcel.readInt())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(titleResource)
        parcel.writeLong(customColor)
        parcel.writeTypedArray(exerciseMetas, flags)
        parcel.writeInt(breakLength)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Workout> {
        override fun createFromParcel(parcel: Parcel): Workout {
            return Workout(parcel)
        }

        override fun newArray(size: Int): Array<Workout?> {
            return arrayOfNulls(size)
        }
    }
}