package com.enjoyingfoss.feeel

import android.os.Parcelable
import com.enjoyingfoss.feeel.model.ExerciseMeta
import com.enjoyingfoss.feeel.model.Workout

/**
@author Miroslav Mazel
 */
interface WorkoutContract {
    interface Presenter {
        fun setHostView(callback: View, savedState: Parcelable?, workout: Workout, ttsEnabled: Boolean)
        fun saveState(): Parcelable?
        fun disconnect(view: WorkoutContract.View) //todo test that this is always called; OR use weakReference instead

        fun skipToPreviousExercise()
        fun skipToNextExercise()

        fun togglePlayPause()
    }

    interface View {
        fun setExercise(exerciseMeta: ExerciseMeta)
        fun setBreak(nextExerciseMeta: ExerciseMeta, breakLength: Int)
        fun showFinish()

        fun setSeconds(seconds: Int) // sets seconds for countdown, both before and during exercise

        fun setPaused()
        fun setPlaying()

        fun close()
    }

    interface Model {
        fun retrieveAll(): List<Workout>
    }
}