package com.enjoyingfoss.feeel.presenter

import java.util.concurrent.Executors
import java.util.concurrent.Future
import java.util.concurrent.TimeUnit

/**
@author Miroslav Mazel
 */

class WorkoutTimer(private val callback: TimerCallback, initTime: Int) {

    interface TimerCallback {
        fun onSecondDecrease()
        fun onTimerZero()
    }

    var timeRemaining = initTime
    private val exerciseExecutor = Executors.newScheduledThreadPool(1)
    private var timerFuture: Future<*>? = null
    var running = false

    fun start() {
        if (timerFuture == null) { //todo double-check if using futures right
            timerFuture = exerciseExecutor.scheduleAtFixedRate({ countSecond() }, 0, 1, TimeUnit.SECONDS) //todo is a try catch needed with the runnable?
            running = true
        }
    }

    fun stop() {
        timerFuture?.cancel(true) //todo double-check if using futures right
        timerFuture = null
        running = false
    }

    fun close() {
        exerciseExecutor.shutdown()
    }

    private fun countSecond() {
        if (timeRemaining == 1) {
            timeRemaining = 0
            callback.onTimerZero()
        } else if (timeRemaining > 0) {
            timeRemaining--
            callback.onSecondDecrease()
        }
    }
}