package com.enjoyingfoss.feeel.view

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.enjoyingfoss.feeel.R
import com.enjoyingfoss.feeel.model.Workout
import kotlinx.android.synthetic.main.activity_cover.*




/**
@author Miroslav Mazel
 */

//todo add license info
class CoverActivity : AppCompatActivity() {
    companion object {
        const val WORKOUT_KEY = "workout"
    }

    override fun onCreate(savedInstanceState: Bundle?) { //todo connect service and preload here
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cover)

        val workout = intent.getParcelableExtra(WORKOUT_KEY) as Workout
        titleTV.setText(workout.titleResource)

//        todo val editor = sharedpreferences.edit()
//        editor.putString("key", "value")
//        editor.commit()

        startExerciseButton.setOnClickListener {
            val startIntent = Intent(this, WorkoutActivity::class.java)
            startIntent.putExtra(WorkoutActivity.TTS_KEY, enableTTS.isChecked)
            startIntent.putExtra(WorkoutActivity.WORKOUT_KEY, workout)
            startActivity(startIntent)
        }
    }
    //todo consider creating service here, then just passing it onto the activity created
}