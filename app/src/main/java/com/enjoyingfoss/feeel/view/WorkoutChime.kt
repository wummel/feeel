package com.enjoyingfoss.feeel.view

import android.content.Context
import android.media.AudioManager
import android.media.SoundPool
import android.os.Build
import com.enjoyingfoss.feeel.R
import com.enjoyingfoss.feeel.WorkoutContract
import com.enjoyingfoss.feeel.model.ExerciseMeta
import java.lang.ref.WeakReference

class WorkoutChime(context: WeakReference<Context>) : WorkoutContract.View {
    @Suppress("DEPRECATION")
    private val soundPool =
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) SoundPool.Builder().build()
            else SoundPool(2, AudioManager.STREAM_MUSIC, 100)
    private val soundVolume = 1f
    private val soundStart = soundPool.load(context.get(), R.raw.exercise_start, 1)
    private val soundBreak = soundPool.load(context.get(), R.raw.exercise_break, 1)
    private val soundFinish = soundPool.load(context.get(), R.raw.exercise_finish, 1)
    private val soundTick = soundPool.load(context.get(), R.raw.time_tick, 1)
    private val countdown = 5

    private var chimeLoaded = false

    init {
        soundPool.setOnLoadCompleteListener { _, _, _ -> chimeLoaded = true }
    }

    override fun setExercise(exerciseMeta: ExerciseMeta) {
        soundPool.play(soundStart, soundVolume, soundVolume, 1, 0, 1f)
    }

    override fun setBreak(nextExerciseMeta: ExerciseMeta, breakLength: Int) {
        soundPool.play(soundBreak, soundVolume, soundVolume, 1, 0, 1f)
    }

    override fun showFinish() {
        soundPool.play(soundFinish, soundVolume, soundVolume, 1, 0, 1f)
    }

    override fun setSeconds(seconds: Int) {
        if (seconds <= countdown)
            soundPool.play(soundTick, soundVolume, soundVolume, 1, 0, 1f)
    }

    override fun setPaused() {}

    override fun setPlaying() {}

    override fun close() {
        soundPool.release()
    }
}