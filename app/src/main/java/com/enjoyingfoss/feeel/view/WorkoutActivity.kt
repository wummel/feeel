package com.enjoyingfoss.feeel.view

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.graphics.ColorMatrix
import android.graphics.ColorMatrixColorFilter
import android.media.AudioManager
import android.os.Build
import android.os.Bundle
import android.os.IBinder
import android.os.Parcelable
import android.support.v7.app.AppCompatActivity
import android.support.v7.content.res.AppCompatResources
import android.text.Html
import android.util.TypedValue
import android.view.View
import android.view.WindowManager
import com.enjoyingfoss.feeel.R
import com.enjoyingfoss.feeel.WorkoutContract
import com.enjoyingfoss.feeel.model.ExerciseMeta
import com.enjoyingfoss.feeel.model.Workout
import com.enjoyingfoss.feeel.presenter.WorkoutService
import kotlinx.android.synthetic.main.activity_workout.*
import java.lang.ref.WeakReference




//todo license info
//todo separate activity without workout
//todo force media playback audio controls at all times on this activity
//todo use a pager, preload next exercises
//todo make sure that an empty description still covers the whole width


//TODO add view stuff from contract

class WorkoutActivity : AppCompatActivity(), ServiceConnection, WorkoutContract.View {
    override fun close() {
        throw IllegalStateException("Can't close a WorkoutContract.View that's an Activity, as it has its own lifecycle")
    }
    //todo implement transition based on https://www.thedroidsonroids.com/blog/android/meaningful-motion-with-shared-element-transition-and-circular-reveal-animation/ or https://guides.codepath.com/android/Circular-Reveal-Animation

    companion object {
        const val TTS_KEY = "audio"
        const val WORKOUT_KEY = "workout"

        private const val STATE_KEY = "STATE"
        private const val CONTROLS_KEY = "CONTROLS"
    }

    //todo do a pager view, preload
    private var presenterService: WeakReference<WorkoutService>? = null
    private var controlsShown = false //todo not happy with this, seems hackish, only there to fix a bug with disappearing controls
    private var restoredState: Parcelable? = null
    private val grayscaleMatrix = ColorMatrix()
    private val grayscaleFilter: ColorMatrixColorFilter
    private var isImageFlipped = false
    private var ttsEnabled = false
    private var workout: Workout? = null

    init {
        grayscaleMatrix.setSaturation(0f)
        grayscaleFilter = ColorMatrixColorFilter(grayscaleMatrix)
    }

    //
    // Lifecycle
    //

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        //todo fails when running in background, but activity card swiped away

        volumeControlStream = AudioManager.STREAM_MUSIC
        setContentView(R.layout.activity_workout)
        val collapseDrawable = AppCompatResources.getDrawable(this, R.drawable.ic_expand_up)
        titleButton.setCompoundDrawablesWithIntrinsicBounds(null, null, collapseDrawable, null)

        ttsEnabled = intent.getBooleanExtra(TTS_KEY, false)
        workout = intent.getParcelableExtra(WORKOUT_KEY)
//        workout?.customColor?.toInt()?.let { headerBox.setBackgroundColor(it) } //todo make sure color is applied on restore as well

        if (savedInstanceState != null) {
            restoredState = savedInstanceState.get(STATE_KEY) as Parcelable?

            controlsShown = savedInstanceState.get(CONTROLS_KEY) as Boolean
            if (controlsShown)
                showControls()
        }

        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
    }

    override fun onStart() {
        super.onStart()

        bindService(
                Intent(this, WorkoutService::class.java),
                this, Context.BIND_AUTO_CREATE
        )
    }

    override fun onStop() {
        super.onStop()
        startService(Intent(this, WorkoutService::class.java)) //todo perhaps not at all necessary
        presenterService?.get()?.disconnect(this)
        unbindService(this)
    }

    override fun onBackPressed() {
        presenterService?.get()?.disconnectOnUnbind = true
        super.onBackPressed()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        restoredState = presenterService?.get()?.saveState()
        outState.putParcelable(STATE_KEY, restoredState)
        outState.putBoolean(CONTROLS_KEY, controlsShown)
    }

    //
    // Service connection
    //

    override fun onServiceDisconnected(p0: ComponentName?) {
        presenterService = null
        //todo unset onclicklisteners needed?
    }

    override fun onServiceConnected(p0: ComponentName?, binder: IBinder) {
        presenterService = (binder as WorkoutService.WorkoutBinder).service

        binder.service.get()!!.setHostView(callback = this, savedState = restoredState, workout = workout!!, ttsEnabled = ttsEnabled)
        setOnClickListeners(binder.service.get())
    }

    private fun setOnClickListeners(service: WorkoutService?) {
        runOnUiThread {
            headerBox.setOnClickListener {
                //todo see if this covers the overlaid text as well —— I don't think it does, but it should
                service?.togglePlayPause()
            }

            exerciseImage.setOnClickListener {
                service?.togglePlayPause()
            }

            playPauseButton.setOnClickListener {
                service?.togglePlayPause()
            }

            previousButton.setOnClickListener {
                service?.skipToPreviousExercise()
            }

            nextButton.setOnClickListener {
                //todo enable swiping through pages to get to next and prev. exercise, too, though only in this mode
                service?.skipToNextExercise()
            }
        }
    }

    //
    // WorkoutContract.View
    //
    override fun setExercise(exerciseMeta: ExerciseMeta) { //todo rejig architecture, I don't like checking for stage all the timeZ
        val exercise = exerciseMeta.exercise
        runOnUiThread {
            titleButton.text = getString(exercise.titleResource)
            descriptionText.text =
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
                        Html.fromHtml(getString(exercise.descResource), Html.FROM_HTML_MODE_LEGACY)
                    else
                        Html.fromHtml(getString(exercise.descResource))
            exerciseImage.setImageResource(exercise.imageResource)

            if (exerciseMeta.isFlipped && !isImageFlipped) {
                exerciseImage.scaleX = -1f
            } else if (!exerciseMeta.isFlipped) { //todo check if this is performant and covers all options
                exerciseImage.scaleX = 1f
            }

            exerciseImage.colorFilter = null
        }
    }

    override fun setBreak(nextExerciseMeta: ExerciseMeta, breakLength: Int) {
        val nextExercise = nextExerciseMeta.exercise
        runOnUiThread {

            titleButton.text = String.format(
                    getString(R.string.next_label),
                    getString(nextExercise.titleResource)
            )
            descriptionText.text =
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
                        Html.fromHtml(getString(nextExercise.descResource), Html.FROM_HTML_MODE_LEGACY)
                    else
                        Html.fromHtml(getString(nextExercise.descResource))
            exerciseImage.setImageResource(nextExercise.imageResource)

            exerciseImage.colorFilter = grayscaleFilter
        }
    }

    override fun showFinish() {
        finish()
    }

    override fun setSeconds(seconds: Int) = runOnUiThread {
        timeText.text = seconds.toString()
    }

    override fun setPlaying() {
        controlsShown = false
        runOnUiThread {
            playPauseButton.visibility = View.GONE
            previousButton.visibility = View.GONE
            nextButton.visibility = View.GONE
            tapIndicator.visibility = View.VISIBLE
            timeText.setTextSize(TypedValue.COMPLEX_UNIT_PX, resources.getDimension(R.dimen.time_headline))

            if (Build.VERSION.SDK_INT >= 16)
                exerciseImage.imageAlpha = 255
            else
                exerciseImage.setAlpha(255)
        }
    }

    override fun setPaused() { //todo visibility failing on rotate sometime! I guess setPaused isn't called sometime!!!
        controlsShown = true
        showControls()

        runOnUiThread {
            tapIndicator.visibility = View.GONE
            timeText.setTextSize(TypedValue.COMPLEX_UNIT_PX, resources.getDimensionPixelSize(R.dimen.time_headline_small).toFloat())

            if (Build.VERSION.SDK_INT >= 16)
                exerciseImage.imageAlpha = 128
            else
                exerciseImage.setAlpha(128)
        }
    }

    private fun showControls() {
        runOnUiThread {
            playPauseButton.visibility = View.VISIBLE
            previousButton.visibility = View.VISIBLE
            nextButton.visibility = View.VISIBLE
        }
    }

    //
    // Custom display
    //

    fun toggleDescription(view: View) {
        runOnUiThread {
            if (descriptionFrame.visibility == View.GONE) {
                descriptionFrame.visibility = View.VISIBLE
                val downCollapseDrawable = AppCompatResources.getDrawable(this, R.drawable.ic_collapse_down)
                titleButton.setCompoundDrawablesWithIntrinsicBounds(null, null, downCollapseDrawable, null)
            } else {
                descriptionFrame.visibility = View.GONE
                val upCollapseDrawable = AppCompatResources.getDrawable(this, R.drawable.ic_expand_up)
                titleButton.setCompoundDrawablesWithIntrinsicBounds(null, null, upCollapseDrawable, null)
            }
        }
    }
}