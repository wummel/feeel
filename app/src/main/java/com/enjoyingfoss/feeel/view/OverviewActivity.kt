package com.enjoyingfoss.feeel.view

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.enjoyingfoss.feeel.R
import com.enjoyingfoss.feeel.model.Workout
import com.enjoyingfoss.feeel.model.WorkoutRepository
import kotlinx.android.synthetic.main.activity_overview.*
import java.lang.ref.WeakReference


class OverviewActivity : AppCompatActivity() {
    //todo make sure the exercise view loads back if service is in background

    override fun onCreate(savedInstanceState: Bundle?) { //todo connect service and preload here
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_overview)

        workoutRecycler.layoutManager = LinearLayoutManager(this)
        workoutRecycler.adapter = WorkoutAdapter(WorkoutRepository.retrieveAll(), WeakReference(workoutRecycler))
        workoutRecycler.addOnItemTouchListener(object: RecyclerView.SimpleOnItemTouchListener() {

        })
    }

    internal class WorkoutAdapter(private val workouts: List<Workout>, private val recyclerView: WeakReference<RecyclerView>)
        : RecyclerView.Adapter<WorkoutAdapter.ViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val view = LayoutInflater
                    .from(parent.context)
                    .inflate(R.layout.item_workout, parent, false)
            view.setOnClickListener {
                val recycler = recyclerView.get()

                val itemPosition = recycler?.getChildAdapterPosition(view) ?: 0
                val workout = workouts[itemPosition]

                val startIntent = Intent(recycler?.context, CoverActivity::class.java)
                startIntent.putExtra(CoverActivity.WORKOUT_KEY, workout)
                recycler?.context?.startActivity(startIntent)
            }
            return ViewHolder(view)
        }

        override fun getItemCount() = workouts.size

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder.title.setText(workouts[position].titleResource)
        }

        internal class ViewHolder(item: View) : RecyclerView.ViewHolder(item) {
            var title = item.findViewById<View>(R.id.title) as TextView
        }
    }
    //todo consider creating service here, then just passing it onto the activity created
}